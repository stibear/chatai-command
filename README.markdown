# chatai-command

## Usage

`shly run "sentence"`

* debug
  `--debug t`

## Require

* sbcl
* shly
* sqlite3
* mecab

## Installation

### Install all the above.

### Get the log, and put it in `db/`

```
$ wget https://www.dropbox.com/s/e68xu0za5xe7j8e/chatailog
$ mv chatailog db/
```

### Setup Mecab.

### Put your mecab dictionary in `dic/`

```
$ mv YOUR_DICT dic/
```

ここでREADMEは終わっている…

## Author

* stibear

## Copyright

Copyright (c) 2015 stibear

