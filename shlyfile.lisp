;; -*- mode: common-lisp -*-
(dolist (sys '(:chatai-command :split-sequence))
  (require sys))

(import 'split-sequence:split-sequence)

#+sbcl(setf sb-impl::*default-external-format* :utf-8)
#+sbcl(setf sb-alien::*default-c-string-external-format* :utf-8)

(defun run (sentence &key debug)
  (let ((std *standard-output*))
    (declare (ignorable std))
    (with-output-to-string (out)
      (let ((*standard-output* out))
        (format std "~A~%"
                (chatai-command:run sentence :debug debug))))
    (values)))
