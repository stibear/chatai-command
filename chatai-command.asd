(in-package :cl-user)
(defpackage chatai-command-asd
  (:use :cl :asdf))
(in-package :chatai-command-asd)

(defsystem chatai-command
  :version "0.1"
  :author "stibear"
  :license ""
  :depends-on (:cl-ppcre
               ;; for DB
               :datafly
               :sxql

               ;; Shell
               :trivial-shell)
  :components ((:module "src"
                :components
                ((:file "chatai" :depends-on ())
                 (:file "mecab" :depends-on ("chatai"))
                 (:file "chatai-db" :depends-on ("chatai"))
                 (:file "util" :depends-on ("chatai"))
                 (:file "main" :depends-on ("chatai")))))
  :description "")
