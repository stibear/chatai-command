(in-package :chatai-command.chatai)

(defun mappend (function &rest lists)
  (apply #'append (apply #'mapcar function lists)))

(defun 2-gram (string)
  (loop :for i :below (1- (length string))
     :collect (subseq string i (+ i 2))))

(defun group (source n)
  (if (zerop n) (error "zero length"))
  (labels ((rec (source acc)
             (let ((rest (nthcdr n source)))
               (if (consp rest)
                   (rec rest (cons
                              (subseq source 0 n)
                              acc))
                   (nreverse
                    (cons source acc))))))
    (if source (rec source nil) nil)))
