(in-package :chatai-command.chatai)

(defun run-mecab (string)
  (with-input-from-string (in string)
    (shell-command (concatenate 'string "mecab -u "
                                (format nil "~a" *mecab-user-dic*))
                   :input in)))

(defun mecab (string)
  (with-input-from-string (in (run-mecab string))
    (loop :for i = (read-line in) :until (string= "EOS" i)
       :collect
       (register-groups-bind (hyoso hinshi rest) ("(.*)\\t(.*?),(.*)" i)
         (list hyoso hinshi rest)))))
