(in-package :chatai-command.chatai)

(connect-toplevel :sqlite3 :database-name
                  (asdf:system-relative-pathname
                   :chatai-command #P"db/chatailog"))

(defstruct chatai-log
  id sentence meishi h-list rowid)

(defun last-rowid ()
  (second (retrieve-one (select ((:max :rowid)) (from :log)))))

(defun select-nth (n &optional to)
  (if to
      (retrieve-all
       (select (:* :rowid) (from :log)
               (where (:and (:<= n :rowid) (:<= :rowid to))))
       :as 'chatai-log)
      (retrieve-one
       (select (:* :rowid) (from :log)
               (where (:= :rowid n)))
       :as 'chatai-log)))

(defun select-nth-list (nth-list)
  (if (> (list-length nth-list) 500)
      (let ((nth-lists (group nth-list 500)))
        (loop
           :for nth-list :in nth-lists
           :append (retrieve-all
                    (select (:* :rowid) (from :log)
                            (where `(:or ,@(loop :for i :in nth-list
                                              :collect (list := :rowid i)))))
                    :as 'chatai-log)))
      (retrieve-all
       (select (:* :rowid) (from :log)
               (where `(:or ,@(loop :for i :in nth-list
                                 :collect (list := :rowid i)))))
       :as 'chatai-log)))

(defun push-to-db (sentence hiragana-list)
  (execute (insert-into :log
             (set= :sentence sentence
                   :h_list (format nil "~s" hiragana-list)))))

