(in-package :cl-user)
(defpackage :chatai-command.chatai
  (:use :cl
        :sxql
        :datafly
        :trivial-shell
        :cl-ppcre)
  (:export :chatai))
(in-package :chatai-command.chatai)

(defparameter *number-of-select* 1000)
(defparameter *mecab-user-dic*
  (asdf:system-relative-pathname :chatai-command #P"dic/user.dic"))

(setf *random-state* (make-random-state t))

(defun chatai (string &key (mode :release))
  (let* ((hiragana-list (hiragana-list string))
         (base-sentence)
         (chosen-noun (random-choose-noun (sentence-alist string))))
    (unless (eq mode :debug) (push-to-db string hiragana-list))
    (when hiragana-list
      (loop
         :with log-sent
         :until log-sent
         :do
         (setf log-sent
               (max-of-score hiragana-list
                             (select-nth-list
                              (random-num-list *number-of-select*))))
         :finally (setf base-sentence log-sent))
      (let ((replaced-noun
             (random-choose-noun (sentence-alist
                                  (chatai-log-sentence base-sentence)))))
        (when (and chosen-noun replaced-noun)
          (regex-replace-all replaced-noun
                             (chatai-log-sentence base-sentence)
                             chosen-noun))))))

(defun sentence-alist (string)
  (mapcar #'(lambda (str) (cons (car str) (cadr str)))
          (mecab string)))

(defun random-choose-noun (sentence-alist)
  (let ((noun-list (loop :for i :in sentence-alist
                      :when (and (string= "名詞" (cdr i))
                                 (not (and (scan "[ぁ-ゞ\x20-\x7E]+" (car i))
                                           (= 1 (length (car i))))))
                      :collect (car i))))
    (nth (if (not (null noun-list))
             (random (length noun-list) *random-state*)
             0)
         noun-list)))

(defun hiragana-list (string)
  (mappend #'2-gram
	   (all-matches-as-strings "[ぁ-ゞァ-ヾ,、.。]+" string)))

(defun max-of-score (hiragana-list log-sentences)
  (loop
     :for sent :in log-sentences
     :with max-score = 0
     :with result
     :do
     (let ((score
            (scoring-hiragana-list hiragana-list
                                   (read-from-string (chatai-log-h-list sent)))))
       (when (< max-score score)
         (setf max-score score
               result sent)))
     :finally (return result)))

(defun hiragana-alist (hiragana-list)
  (select-nth-list (random-num-list *number-of-select*)))

(defun scoring-hiragana-list (h-list1 h-list2)
  (if (or (null h-list1) (null h-list2))
      -1000                             ; no meaning number
      (/ (list-length (intersection h-list1 h-list2 :test #'string=))
         (list-length h-list2))))

(defun random-num-list (n)
  (let ((random-arg (last-rowid)))
    (loop :for i :to (1- n)
       :collect (1+ (random random-arg (make-random-state t))))))
