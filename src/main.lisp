(in-package :cl-user)
(defpackage chatai-command
  (:use :cl
        :chatai-command.chatai)
  (:export :run))
(in-package :chatai-command)

(defun run (sentence &key debug)
  (chatai sentence :mode (if debug :debug :release)))
